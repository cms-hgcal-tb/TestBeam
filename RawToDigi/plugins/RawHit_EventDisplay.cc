#include <iostream>
#include "TH1F.h"
#include "TH2F.h"
#include "TH2Poly.h"
#include <fstream>
#include <sstream>
#include <algorithm>
// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "HGCal/DataFormats/interface/HGCalTBRawHitCollection.h"
#include "HGCal/DataFormats/interface/HGCalTBDetId.h"
#include "HGCal/CondObjects/interface/HGCalElectronicsMap.h"
#include "HGCal/CondObjects/interface/HGCalCondObjectTextIO.h"
#include "HGCal/DataFormats/interface/HGCalTBElectronicsId.h"
#include "HGCal/Geometry/interface/HGCalTBCellVertices.h"
#include "HGCal/Geometry/interface/HGCalTBTopology.h"
#include "HGCal/Geometry/interface/HGCalTBGeometryParameters.h"
#include "HGCal/Reco/interface/CommonMode.h"
#include <iomanip>
#include <set>

using namespace std;

#define MAXVERTICES 6
static const double delta = 0.00001;//Add/subtract delta = 0.00001 to x,y of a cell centre so the TH2Poly::Fill doesnt have a problem at the edges where the centre of a half-hex cell passes through the sennsor boundary line.

class RawHit_EventDisplay : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit RawHit_EventDisplay(const edm::ParameterSet&);
  ~RawHit_EventDisplay();
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
private:
  virtual void beginJob() override;
  void analyze(const edm::Event& , const edm::EventSetup&) override;
  virtual void endJob() override;
  void InitTH2Poly(TH2Poly& poly, int layerID);

  std::string m_electronicMap;

  HGCalElectronicsMap m_emap;
  int m_evtID;

  int m_sensorsize;
  uint16_t m_numberOfBoards;
  CommonMode m_commonMode;
  CommonModeNoiseMethod m_commonMeth;
  std::string m_subtractCommonModeOption;

  edm::EDGetTokenT<HGCalTBRawHitCollection> m_HGCalTBRawHitCollection;

  HGCalTBTopology IsCellValid;
  HGCalTBCellVertices TheCell;
  std::vector<std::pair<double, double>> CellXY;
  std::pair<double, double> CellCentreXY;
  
};

RawHit_EventDisplay::RawHit_EventDisplay(const edm::ParameterSet& iConfig) :
  m_electronicMap(iConfig.getUntrackedParameter<std::string>("ElectronicMap","HGCal/CondObjects/data/map_CERN_Hexaboard_July_6Layers.txt")),
  m_sensorsize(iConfig.getUntrackedParameter<int>("SensorSize",128)),
  m_numberOfBoards(iConfig.getUntrackedParameter<int>("NumberOfBoards",28)),
  m_subtractCommonModeOption(iConfig.getUntrackedParameter<std::string>("CommonModeNoiseMethod","MEDIANPERBOARDWITHTHRESHOLD"))
{
  m_HGCalTBRawHitCollection = consumes<HGCalTBRawHitCollection>(iConfig.getParameter<edm::InputTag>("InputCollection"));
  
  m_evtID=0;
  
  std::cout << iConfig.dump() << std::endl;
}


RawHit_EventDisplay::~RawHit_EventDisplay()
{

}

void RawHit_EventDisplay::beginJob()
{
  HGCalCondObjectTextIO io(0);
  edm::FileInPath fip(m_electronicMap);
  if (!io.load(fip.fullPath(), m_emap)) {
    throw cms::Exception("Unable to load electronics map");
  };

  usesResource("TFileService");
  edm::Service<TFileService> fs;

  m_commonMeth=NOCMSUBTRACTION;
  if ( m_subtractCommonModeOption == "MEDIANPERBOARDWITHTHRESHOLD" )
    m_commonMeth=MEDIANPERBOARDWITHTHRESHOLD;
  else if ( m_subtractCommonModeOption == "MEDIANPERBOARD" )
    m_commonMeth=MEDIANPERBOARD;
  else if ( m_subtractCommonModeOption == "MEDIANPERCHIP" )
    m_commonMeth=MEDIANPERCHIP;

  m_commonMode=CommonMode(m_emap,m_commonMeth);
}

void RawHit_EventDisplay::analyze(const edm::Event& event, const edm::EventSetup& setup)
{
  usesResource("TFileService");
  edm::Service<TFileService> fs;

  edm::Handle<HGCalTBRawHitCollection> hits;
  event.getByToken(m_HGCalTBRawHitCollection, hits);
  if( !hits->size() )
    return;
  
  std::map<int,TH2Poly*>  polyMap;
  std::ostringstream os( std::ostringstream::ate );
  os << "Event" << event.id().event();
  TFileDirectory dir = fs->mkdir( os.str().c_str() );
  for(size_t ib = 0; ib<m_numberOfBoards; ib++) {
    for(int it=0; it<6; it++){
      TH2Poly *h=dir.make<TH2Poly>();
      os.str("");
      os<<"Board"<<ib<<"_TimeSample"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib);
      polyMap.insert( std::pair<int,TH2Poly*>(100*ib + it, h) );
    }
  }
  m_commonMode.Evaluate( hits );
  std::map<int,commonModeNoise> cmMap=m_commonMode.CommonModeNoiseMap();

  // for( std::map<int,commonModeNoise>::iterator it=cmMap.begin(); it!=cmMap.end(); ++it ){
  //   std::cout << "board = " << it->first << " CM full hg =";
  //   for(int time=0; time<6; time++)
  //     std::cout << " " << it->second.fullHG[time] ;
  //   std::cout << std::endl;
  // }  
  for( auto hit : *hits ){
    HGCalTBElectronicsId eid( m_emap.detId2eid(hit.detid().rawId()) );
    int board=hit.skiroc()/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
    int cmKey = hit.skiroc();
    if( m_commonMeth ==  MEDIANPERBOARD || m_commonMeth== MEDIANPERBOARDWITHTHRESHOLD )
      cmKey=board;
    if( !m_emap.existsEId(eid) || board>=m_numberOfBoards ) continue;
    CellCentreXY=TheCell.GetCellCentreCoordinatesForPlots(hit.detid().layer(), hit.detid().sensorIU(), hit.detid().sensorIV(), hit.detid().iu(), hit.detid().iv(), m_sensorsize);
    double iux = (CellCentreXY.first < 0 ) ? (CellCentreXY.first + delta) : (CellCentreXY.first - delta) ;
    double iuy = (CellCentreXY.second < 0 ) ? (CellCentreXY.second + delta) : (CellCentreXY.second - delta);
    for(int it=0; it<6; it++)
      polyMap[ 100*(hit.skiroc()/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)+it ]->Fill(iux , iuy, hit.highGainADC(it)-cmMap[cmKey].fullHG[it]);
  }
}

void RawHit_EventDisplay::InitTH2Poly(TH2Poly& poly, int layerID)
{
  double HexX[MAXVERTICES] = {0.};
  double HexY[MAXVERTICES] = {0.};
  for(int iv = -7; iv < 8; iv++) {
    for(int iu = -7; iu < 8; iu++) {
      if(!IsCellValid.iu_iv_valid(layerID, 0, 0, iu, iv, m_sensorsize)) 
	continue;
      CellXY = TheCell.GetCellCoordinatesForPlots(layerID, 0, 0, iu, iv, m_sensorsize);
      assert(CellXY.size() == 4 || CellXY.size() == 6);
      unsigned int iVertex = 0;
      for(std::vector<std::pair<double, double>>::const_iterator it = CellXY.begin(); it != CellXY.end(); it++) {
	HexX[iVertex] =  it->first;
	HexY[iVertex] =  it->second;
	++iVertex;
      }
      //Somehow cloning of the TH2Poly was not working. Need to look at it. Currently physically booked another one.
      poly.AddBin(CellXY.size(), HexX, HexY);
    }//loop over iu
  }//loop over iv
}


void RawHit_EventDisplay::endJob()
{
  usesResource("TFileService");
  edm::Service<TFileService> fs;
}

void RawHit_EventDisplay::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

DEFINE_FWK_MODULE(RawHit_EventDisplay);
