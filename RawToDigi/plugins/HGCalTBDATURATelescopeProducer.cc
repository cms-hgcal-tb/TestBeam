#include "HGCal/RawToDigi/plugins/HGCalTBDATURATelescopeProducer.h"

//#define DEBUG

HGCalTBDATURATelescopeProducer::HGCalTBDATURATelescopeProducer(const edm::ParameterSet& cfg) {
    RunDataToken = consumes<RunData>(cfg.getParameter<edm::InputTag>("RUNDATA"));
    inputFile = cfg.getParameter<std::string>("inputFile");
    outputCollectionName = cfg.getParameter<std::string>("OutputCollectionName");
    SkipFirstNEvents = cfg.getParameter<int>("SkipFirstNEventsInTelescopeFile");
    m_layerPositionFile = cfg.getParameter<std::string>("layerPositionFile");
    m_considerAirInGBLTracking = cfg.getParameter<bool>("considerAirInGBLTracking");
    m_beamEnergyForGBLTracking = cfg.getParameter<double>("beamEnergyForGBLTracking");  //in GeV
    m_PIStagePositionFile = cfg.getParameter<std::string>("PIStagePositionFile");


    produces<std::vector<HGCalTBDATURATelescopeData> >(outputCollectionName);
    produces<RunData>("FullRunData");
}

void HGCalTBDATURATelescopeProducer::beginJob() {
    rootFile = new TFile(inputFile.c_str(), "READ");
    tree = (TTree*)rootFile->Get("corryvreckan/HGCalTBDataOutput/trackClusters");           //hard coded value, might be subject to change

    tree->SetBranchAddress("EventID", &tree_eventID, &b_event);
    tree->SetBranchAddress("chi2", &tree_track_chi2, &b_chi2);
    tree->SetBranchAddress("NTracks", &tree_Ntracks, &b_Ntracks);

    for (int MIMOSA_index = 1; MIMOSA_index <= 6; MIMOSA_index++) {
        b_clusterX[MIMOSA_index] = new TBranch;
        b_clusterY[MIMOSA_index] = new TBranch;
        b_clusterZ[MIMOSA_index] = new TBranch;
        b_absorber[MIMOSA_index] = new TBranch;


        tree->SetBranchAddress(("associatedClusterX_MIMOSA26_" + std::to_string(MIMOSA_index)).c_str(), &tree_clusterX[MIMOSA_index], &b_clusterX.at(MIMOSA_index));
        tree->SetBranchAddress(("associatedClusterY_MIMOSA26_" + std::to_string(MIMOSA_index)).c_str(), &tree_clusterY[MIMOSA_index], &b_clusterY.at(MIMOSA_index));
        tree->SetBranchAddress(("associatedClusterZ_MIMOSA26_" + std::to_string(MIMOSA_index)).c_str(), &tree_clusterZ[MIMOSA_index], &b_clusterZ.at(MIMOSA_index));
        tree->SetBranchAddress(("associatedAbsorber_MIMOSA26_" + std::to_string(MIMOSA_index)).c_str(), &tree_absorber[MIMOSA_index], &b_absorber.at(MIMOSA_index));

        tree_clusterX[MIMOSA_index] = 0;
        tree_clusterY[MIMOSA_index] = 0;
        tree_clusterZ[MIMOSA_index] = 0;
        tree_absorber[MIMOSA_index] = 0;

    }

    tree_track_chi2 = 0;    //set pointer to vector to 0


    //reads the layer positions
    NHGCalLayers = 0;
    std::fstream file;
    char fragment[100];
    int readCounter = -1;

    file.open(m_layerPositionFile.c_str(), std::fstream::in);
    std::cout << "Reading file " << m_layerPositionFile << " -open: " << file.is_open() << std::endl;
    int layer = 0;
    double z_position_layer = -1;
    double X0_layer = -1;
    while (file.is_open() && !file.eof()) {
        readCounter++;
        file >> fragment;
        if (readCounter == 0) layer = atoi(fragment);
        else if (readCounter == 1) z_position_layer = atof(fragment) / 10.;   //values are given in mm and should be converted into cm
        else if (readCounter == 2) {
            X0_layer = atof(fragment);
            layerPositions[layer] = std::make_pair(z_position_layer, X0_layer);
            readCounter = -1;
            NHGCalLayers++;
        }
    }
    file.close();

    //reads the PI stage positions
    readCounter = -1;
    file.open(m_PIStagePositionFile.c_str(), std::fstream::in);
    std::cout << "Reading file " << m_PIStagePositionFile << " -open: " << file.is_open() << std::endl;
    int run = 0;
    float pos_x = 0;
    float pos_y = 0;
    while (file.is_open() && !file.eof()) {
        readCounter++;
        file >> fragment;
        if (readCounter == 0) continue;
        if (readCounter == 1) run = atoi(fragment);
        if (readCounter == 2) continue;
        if (readCounter == 3) pos_x = atof(fragment);
        if (readCounter == 4) continue;
        if (readCounter == 5) {
            pos_y = atof(fragment);
            PIStagePositions[run] = std::make_pair(pos_x, pos_y);
            readCounter = -1;
        }
    }


    //prepare the residual histograms
    edm::Service<TFileService> fs;
    std::ostringstream os( std::ostringstream::ate );
    for (int MIMOSA_index = 1; MIMOSA_index <= 6; MIMOSA_index++) {
        os.str(""); os << "MIOSA_plane" << MIMOSA_index << "__residuals__triplet_track";
        DATURA_residuals_triplet_track[MIMOSA_index] = fs->make<TH2F>(os.str().c_str(), os.str().c_str(), 1000, -0.1, 0.1, 1000, -0.1, 0.1);
        os.str(""); os << "MIOSA_plane" << MIMOSA_index << "__residuals__full_track";
        DATURA_residuals_full_track[MIMOSA_index] = fs->make<TH2F>(os.str().c_str(), os.str().c_str(), 1000, -0.1, 0.1, 1000, -0.1, 0.1);
        os.str(""); os << "MIOSA_plane" << MIMOSA_index << "__residuals__GBL_track";
        DATURA_residuals_GBL_track[MIMOSA_index] = fs->make<TH2F>(os.str().c_str(), os.str().c_str(), 1000, -0.1, 0.1, 1000, -0.1, 0.1);
    }
}

void HGCalTBDATURATelescopeProducer::produce(edm::Event& event, const edm::EventSetup& iSetup) {

    //get the relevant event information
    edm::Handle<RunData> rd;
    event.getByToken(RunDataToken, rd);

    std::unique_ptr<std::vector<HGCalTBDATURATelescopeData> > DATURATracks(new std::vector<HGCalTBDATURATelescopeData>);

    tree->GetEntry(rd->event - 1 + SkipFirstNEvents);
#ifdef DEBUG
    std::cout << "Run event: " << rd->event << "  vs. eventID in tree: " << tree_eventID << "   Number of tracks:" << tree_Ntracks << std::endl;
    for (int nt = 0; nt < tree_Ntracks; nt++) {
        std::cout << "Track " << nt + 1 << ": " << tree_track_chi2->at(nt) << std::endl;
        for (int MIMOSA_index = 1; MIMOSA_index <= 6; MIMOSA_index++) {
            std::cout << tree_clusterX[MIMOSA_index]->at(nt) << "  ,  " << tree_clusterY[MIMOSA_index]->at(nt) << "  ,  " << tree_clusterZ[MIMOSA_index]->at(nt) << std::endl;
        }
    }
#endif

    std::unique_ptr<RunData> rd_full(new RunData);
    rd_full->configuration = rd->configuration;
    rd_full->run = rd->run;
    rd_full->trigger = rd->trigger;
    rd_full->event = rd->event;
    rd_full->energy = rd->energy;
    rd_full->runType = rd->runType;
    rd_full->pdgID = rd->pdgID;
    if (rd->booleanUserRecords.has("hasDanger")) rd_full->booleanUserRecords.add("hasDanger", rd->booleanUserRecords.get("hasDanger"));
    if (rd->doubleUserRecords.has("trueEnergy")) rd_full->doubleUserRecords.add("trueEnergy", rd->doubleUserRecords.get("trueEnergy"));

    if (PIStagePositions.find((int)rd->run) != PIStagePositions.end()) {
        rd_full->doubleUserRecords.add("PIStagePosition_X", PIStagePositions[(int)rd->run].first);
        rd_full->doubleUserRecords.add("PIStagePosition_Y", PIStagePositions[(int)rd->run].second);
    }


    if (tree_Ntracks < 1) {
        rd_full->booleanUserRecords.add("hasValidDATURAMeasurement", false);
    }
    else {
        for (int ntrack = 1; ntrack <= tree_Ntracks; ntrack++) {

            HGCalTBDATURATelescopeData DATURATelescopeTrack(ntrack);

            //triplet tracks
            LineFitter TripletTrack1X;
            LineFitter TripletTrack1Y;
            LineFitter TripletTrack2X;
            LineFitter TripletTrack2Y;
            //division by ten to convert into mm
            for (int MIMOSA_index = 1; MIMOSA_index <= 3; MIMOSA_index++) {
                TripletTrack1X.addPoint(tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION);
                TripletTrack1Y.addPoint(tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION);
            }
            for (int MIMOSA_index = 4; MIMOSA_index <= 6; MIMOSA_index++) {
                TripletTrack2X.addPoint(tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION);
                TripletTrack2Y.addPoint(tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION);
            }
            TripletTrack1X.fit();       TripletTrack1Y.fit();
            TripletTrack2X.fit();       TripletTrack2Y.fit();



            for (std::map<int, std::pair<double, double> >::iterator layerIt = layerPositions.begin(); layerIt != layerPositions.end(); layerIt++) {
                double layer_ref_x = TripletTrack2X.eval(layerIt->second.first);
                double layer_ref_y = TripletTrack2Y.eval(layerIt->second.first);
                double layer_ref_x_chi2 = TripletTrack2X.GetChisquare();
                double layer_ref_y_chi2 = TripletTrack2Y.GetChisquare();

                if (layerIt->first == 1) {
                    layer_ref_x += TripletTrack1X.eval(layerIt->second.first);
                    layer_ref_y += TripletTrack1Y.eval(layerIt->second.first);
                    layer_ref_x_chi2 += TripletTrack1X.GetChisquare();
                    layer_ref_y_chi2 += TripletTrack1Y.GetChisquare();

                    layer_ref_x /= 2.;
                    layer_ref_y /= 2.;
                    layer_ref_x_chi2 /= 2.;
                    layer_ref_y_chi2 /= 2.;
                }
#ifdef DEBUG
                std::cout << "Triplet track at layer" << layerIt->first << " : " << layer_ref_x << "," << layer_ref_y << std::endl;
#endif
                DATURATelescopeTrack.addLayerReference(-layerIt->first, layer_ref_x, layer_ref_y, layer_ref_x_chi2, layer_ref_y_chi2);
            }

            float kinkAngleX_DUT1 = atan(TripletTrack1X.getM()) - atan(TripletTrack2X.getM());
            float kinkAngleY_DUT1 = atan(TripletTrack1Y.getM()) - atan(TripletTrack2Y.getM());
            DATURATelescopeTrack.floatUserRecords.add("kinkAngleX_DUT1", kinkAngleX_DUT1);
            DATURATelescopeTrack.floatUserRecords.add("kinkAngleY_DUT1", kinkAngleY_DUT1);



            //full six point tracks
            LineFitter FullTrackX;
            LineFitter FullTrackY;
            for (int MIMOSA_index = 1; MIMOSA_index <= 6; MIMOSA_index++) {
                DATURATelescopeTrack.addPointForTracking(tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION, MIMOSA26_CLUSTER_RESOLUTION);
                FullTrackX.addPoint(tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION);
                FullTrackY.addPoint(tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION);
            }
            FullTrackX.fit();       FullTrackY.fit();

            //GBL tracks
            ParticleTrack gblTrack;

            //Telescope planes 1-3, including air
            for (int MIMOSA_index = 1; MIMOSA_index <= 3; MIMOSA_index++) {
                gblTrack.addFitPoint(MIMOSA_index, tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION, MIMOSA26_CLUSTER_RESOLUTION, MIMOSA26_MATERIAL_BUDGET, m_beamEnergyForGBLTracking);

                if (m_considerAirInGBLTracking && (MIMOSA_index < 3)) {
                    double dz = tree_clusterZ[MIMOSA_index + 1]->at(ntrack - 1) / 10 - tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10;
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 1, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 - 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 2, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 + 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                }
            }

            //Telescope planes 4-6, including air
            for (int MIMOSA_index = 4; MIMOSA_index <= 6; MIMOSA_index++) {
                gblTrack.addFitPoint(MIMOSA_index, tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10, MIMOSA26_CLUSTER_RESOLUTION, MIMOSA26_CLUSTER_RESOLUTION, MIMOSA26_MATERIAL_BUDGET, m_beamEnergyForGBLTracking);

                if (m_considerAirInGBLTracking && (MIMOSA_index < 6)) {
                    double dz = tree_clusterZ[MIMOSA_index + 1]->at(ntrack - 1) / 10 - tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10;
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 1, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 - 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 2, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 + 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                }
            }

            for (std::map<int, std::pair<double, double> >::iterator layerIt = layerPositions.begin(); layerIt != layerPositions.end(); layerIt++) {
                gblTrack.addReferenceSensor(1000 + layerIt->first, layerIt->second.first, layerIt->second.second, m_beamEnergyForGBLTracking);
            }

            if (m_considerAirInGBLTracking) {
                std::map<int, std::pair<double, double> >::iterator layerIt = layerPositions.begin();
                //air between MIMOSA plane 3 and the HGCal module
                int MIMOSA_index = 3;
                double dz = layerIt->second.first - tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10;
                gblTrack.addDummySensor(MIMOSA_index * 100 + 11, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 - 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                gblTrack.addDummySensor(MIMOSA_index * 100 + 12, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 + 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);

                //air between the first HGCal module and MIMOSA plane 4
                MIMOSA_index = 4;
                dz = -(layerIt->second.first - tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10);
                gblTrack.addDummySensor(MIMOSA_index * 100 + 11, layerIt->second.first + dz * (0.5 - 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                gblTrack.addDummySensor(MIMOSA_index * 100 + 12, layerIt->second.first + dz * (0.5 + 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);

                if (NHGCalLayers > 1) {
                    //air between MIMOSA plane 6 and the second HGCal module
                    layerIt++;
                    MIMOSA_index = 6;
                    dz = layerIt->second.first - tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10;
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 11, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 - 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 12, tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10 + dz * (0.5 + 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                }

                if (NHGCalLayers > 2) {
                    //air between calo layer 2 and calo layer 3
                    layerIt++;
                    dz = layerIt->second.first;
                    layerIt--;
                    dz = dz - layerIt->second.first;
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 21, layerIt->second.first + dz * (0.5 - 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                    gblTrack.addDummySensor(MIMOSA_index * 100 + 22, layerIt->second.first + dz * (0.5 + 1. / sqrt(12.)), dz * 10 / X0_Air, m_beamEnergyForGBLTracking);
                }
            }

            gblTrack.fitTrack(GBLTRACK);
#ifdef DEBUG
            std::cout << "From GBL track at layer 1: " << gblTrack.calculatePositionXY(-1, 1001).first << "," << gblTrack.calculatePositionXY(-1, 1001).second << std::endl;
            std::cout << "From GBL track at layer 2: " << gblTrack.calculatePositionXY(-1, 1002).first << "," << gblTrack.calculatePositionXY(-1, 1002).second << std::endl;
            std::cout << "From GBL track at layer 3: " << gblTrack.calculatePositionXY(-1, 1003).first << "," << gblTrack.calculatePositionXY(-1, 1003).second << std::endl;
            std::cout << std::endl;
#endif
            //append to the DATURA track
            for (std::map<int, std::pair<double, double> >::iterator layerIt = layerPositions.begin(); layerIt != layerPositions.end(); layerIt++) {
                DATURATelescopeTrack.addLayerReference(+layerIt->first, gblTrack.calculatePositionXY(-1, 1001).first, gblTrack.calculatePositionXY(-1, 1001).second, gblTrack.getChi2(1), gblTrack.getChi2(2));
            }

            DATURATracks->push_back(DATURATelescopeTrack);


            //compute residuals
            for (int MIMOSA_index = 1; MIMOSA_index <= 6; MIMOSA_index++) {
                double z = tree_clusterZ[MIMOSA_index]->at(ntrack - 1) / 10;

                double x_measurement = tree_clusterX[MIMOSA_index]->at(ntrack - 1) / 10;
                double y_measurement = tree_clusterY[MIMOSA_index]->at(ntrack - 1) / 10;

                double x_pred_triplet_track = (MIMOSA_index <= 3) ? TripletTrack1X.eval(z) : TripletTrack2X.eval(z);
                double y_pred_triplet_track = (MIMOSA_index <= 3) ? TripletTrack1Y.eval(z) : TripletTrack2Y.eval(z);

                double x_pred_full_track = FullTrackX.eval(z);
                double y_pred_full_track = FullTrackY.eval(z);

                double x_pred_GBL_track = gblTrack.calculatePositionXY(z, MIMOSA_index).first;
                double y_pred_GBL_track = gblTrack.calculatePositionXY(z, MIMOSA_index).second;

                DATURA_residuals_triplet_track[MIMOSA_index]->Fill(10 * (x_measurement - x_pred_triplet_track), 10 * (y_measurement - y_pred_triplet_track));
                DATURA_residuals_full_track[MIMOSA_index]->Fill(10 * (x_measurement - x_pred_full_track), 10 * (y_measurement - y_pred_full_track));
                DATURA_residuals_GBL_track[MIMOSA_index]->Fill(10 * (x_measurement - x_pred_GBL_track), 10 * (y_measurement - y_pred_GBL_track));

            }


        }

#ifdef DEBUG
        for (size_t i = 0; i < DATURATracks->size(); i++) std::cout << "Adding a telescope track with " << DATURATracks->at(i).Extrapolation_XY(1).first << ", " << DATURATracks->at(i).Extrapolation_XY(1).second << "   (" << DATURATracks->at(i).chi2_x << ", " << DATURATracks->at(i).chi2_y << ")" << std::endl;
#endif
        rd_full->booleanUserRecords.add("hasValidDATURAMeasurement", true);
    }
    event.put(std::move(rd_full), "FullRunData");
    event.put(std::move(DATURATracks), outputCollectionName);

}


void HGCalTBDATURATelescopeProducer::endJob() {
    delete tree;
    delete rootFile;
}

DEFINE_FWK_MODULE(HGCalTBDATURATelescopeProducer);
