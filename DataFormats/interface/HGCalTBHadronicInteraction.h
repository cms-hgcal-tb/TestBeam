#ifndef HGCALTBHADRONICINTERACTION_H
#define HGCALTBHADRONICINTERACTION_H

#include <utility>
#include <vector>
#include "Rtypes.h"

class  HGCalTBHadronicInteraction {
public:

     HGCalTBHadronicInteraction(): m_nsec(0){};

     double   xHadInt() const {return m_xHadInt;}
     double   yHadInt()  const {return m_yHadInt;}
     double   zHadInt()  const {return m_zHadInt;}
     UInt_t   eventHadInt()  const {return m_eventHadInt;}
     bool     foundHadInt()  const {return m_foundHadInt;}
     int      processHadInt() const {return m_processHadInt;}
     int      nsec() const {return m_nsec;}
     double   sec_pdgID(int iSec) const {return m_sec_pdgID[iSec];}
     double   sec_charge(int iSec) const {return m_sec_charge[iSec];}
     double   sec_kin(int iSec) const {return m_sec_kin[iSec];}
     double   sec_x(int iSec) const {return m_sec_x[iSec];}
     double   sec_y(int iSec) const {return m_sec_y[iSec];}
     double   sec_z(int iSec) const {return m_sec_z[iSec];}

     void     set_xHadInt(double xHadInt) {m_xHadInt = xHadInt;}
     void     set_yHadInt(double yHadInt) {m_yHadInt = yHadInt;}
     void     set_zHadInt(double zHadInt) {m_zHadInt = zHadInt;}
     void     set_eventHadInt(UInt_t eventHadInt) {m_eventHadInt = eventHadInt;}
     void     set_foundHadInt(bool foundHadInt) {m_foundHadInt = foundHadInt;}
     void     set_processHadInt(int processHadInt) {m_processHadInt = processHadInt;}
     void     set_nsec(int nsec) {m_nsec = nsec;}
     void     add_sec_pdgID(double sec_pdgID) {m_sec_pdgID.push_back(sec_pdgID);}
     void     add_sec_charge(double sec_charge) {m_sec_charge.push_back(sec_charge);}
     void     add_sec_kin(double sec_kin) {m_sec_kin.push_back(sec_kin);}
     void     add_sec_x(double sec_x) {m_sec_x.push_back(sec_x);}
     void     add_sec_y(double sec_y) {m_sec_y.push_back(sec_y);}
     void     add_sec_z(double sec_z) {m_sec_z.push_back(sec_z);}

private:
     double        m_xHadInt;
     double        m_yHadInt;
     double        m_zHadInt;
     UInt_t        m_eventHadInt;
     bool          m_foundHadInt;
     int           m_processHadInt;
     int           m_nsec;
     std::vector<double>  m_sec_pdgID;
     std::vector<double>  m_sec_charge;
     std::vector<double>  m_sec_kin;
     std::vector<double>  m_sec_x;
     std::vector<double>  m_sec_y;
     std::vector<double>  m_sec_z;
};

#endif
