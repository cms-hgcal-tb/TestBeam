#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/program_options.hpp>

#include <HGCal/CondObjects/interface/HGCalCondObjects.h>
#include <HGCal/DataFormats/interface/HGCalTBDetId.h>
#include <HGCal/Geometry/interface/HGCalTBGeometryParameters.h>

/*
  This code assumes the following format for the input calibration file:
  Module_ID ASIC_ID Channel ADC_To_MIP LowGain_To_HighGain_Transition LowGain_To_HighGain_Conversion TOT_To_LowGain_Transition TOT_To_LowGain_Conversion TOT_Offset Fully_calibrated
  
  ADC_To_MIP : Conversion factor between HG ADC and MIP : 1 [HG ADC]=ADC_To_MIP [MIP]
  LowGain_To_HighGain_Transition : Saturation point of high gain ADC (in HG unit)
  LowGain_To_HighGain_Conversion : Conversion factor between LG ADC and HG ADC : 1 [LG ADC]=LowGain_To_HighGain_Conversion [HG ADC]
  TOT_To_LowGain_Transition : Saturation point of low gain ADC (in LG unit)
  TOT_To_LowGain_Conversion : Conversion factor between TOT and LG ADC : 1 [TOT]=TOT_To_LowGain_Conversion [LG ADC]
  TOT_Offset Fully_calibrated : flag to know if the calibration is fully done for this channel or if it comes from an average value

 */
int main(int argc,char**argv)
{
  std::string m_calibfilename,m_dbname;
  try { 
    namespace po = boost::program_options; 
    po::options_description desc("Options"); 
    desc.add_options() 
      ("help,h", "Print help messages") 
      ("CalibrationFileName", po::value<std::string>(&m_calibfilename)->default_value("calib.txt"), "name of input calibration file")
      ("DBName", po::value<std::string>(&m_dbname)->default_value("myDB.db"), "name of data base");
      
    po::variables_map vm; 
    try { 
      po::store(po::parse_command_line(argc, argv, desc),  vm); 
      if ( vm.count("help")  ) { 
        std::cout << desc << std::endl; 
        return 0; 
      } 
      po::notify(vm);
    }
    catch(po::error& e) { 
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      std::cerr << desc << std::endl; 
      return 1; 
    }
    if( vm.count("DBName") ) std::cout << "DBName = " << m_dbname << std::endl;
    if( vm.count("CalibrationFileName") ) std::cout << "CalibrationFileName = " << m_calibfilename << std::endl;
    
  }
  catch(std::exception& e) { 
    std::cerr << "Unhandled Exception reached the top of main: " 
              << e.what() << ", application will now exit" << std::endl; 
    return 2; 
  } 

  HGCalTBCondDB db;
  std::ifstream ifs(m_dbname);
  boost::archive::text_iarchive ia(ifs);
  ia >> db;

  FILE* file;
  char buffer[300];
  file = fopen (m_calibfilename.c_str() , "r");
  if (file == NULL){ perror ("Error opening pedestal high gain"); exit(1); }
  else{
    while ( ! feof (file) ){
      if ( fgets (buffer , 300 , file) == NULL ) break;
      const char* index = buffer;
      int module,skiroc,channel,ptr,nval;
      float adctomip, hgsat, hglgconv, lgsat, lgtotconv, totoffset, calibflag;
      nval=sscanf( index, "%d %d %d %f %f %f %f %f %f %f %n",&module,&skiroc,&channel,&adctomip,&hgsat,&hglgconv,&lgsat,&lgtotconv,&totoffset,&calibflag,&ptr );
      if( nval==10 ){
	for( std::vector<HGCalTBCondChannel>::iterator it=db.get().begin(); it!=db.get().end(); ++it ){
	  int chipid = 1+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-(*it).elecid())%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
	  if( (*it).module()==module && chipid==skiroc ){
	    (*it).updateHighGainMIP(adctomip);
	    (*it).updateHighGainSAT(hgsat);
	    (*it).updateLowGainMIP(adctomip*hglgconv) ;
	    (*it).updateLowGainSAT(lgsat);
	    (*it).updateTotCOEF(adctomip*hglgconv*lgtotconv);
	    (*it).updateTotPED(adctomip*hglgconv*totoffset);
	    /*	      
	       Energy unit == MIP
	       **********************

	       Energy = HG*adctomip ( when HG<hgsat )
	       **********************

	       HG_eq = LG*hglgconv
	       Energy = LG*hglgconv*adctomip ( when HG>hgsat && LG<lgsat )
	       **********************

	       LG_eq = totoffset + TOT*lgtotconv
	       HG_eq = (totoffset + TOT*lgtotconv)*hglgconv
	       Energy = (totoffset + TOT*lgtotconv)*hglgconv*adctoMIP ( when LG>lgsat )
	       **********************
	       
	       */
	    if( calibflag )
	      (*it).updateCalibFlag(FullyCalibrated);
	    else 
	      (*it).updateCalibFlag(PartiallyCalibrated);
	    db.update(*it);
	  }
	}
      }
    }
    fclose (file);
  }


  std::ofstream ofs(m_dbname);
  boost::archive::text_oarchive oa(ofs);
  oa << db;
  ofs.close();

  int index=0;
  for( std::vector<HGCalTBCondChannel>::iterator it=db.get().begin(); it!=db.get().end(); ++it ){
    if(index<1000){
      std::cout << (*it) << std::endl;
    }
    index++;
  }

  return 0;
}
